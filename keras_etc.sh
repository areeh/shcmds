#!/usr/bin/env bash

cd /usr/bin/anaconda/bin
source activate py35
pip install tensorflow
pip install keras
conda install scikit-learn
conda install pandas
